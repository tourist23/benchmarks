package ru.rkunz.benchmarks.custom.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rkunz on 03.04.2017.
 */
public class AddingToListBenchmark {

    private final static Object ELEMENT = new Object();

    public static Test createAddingToMiddleArrayListTest(int listSize, int iterationCount){
        return new AddingToMiddleArrayListTest(listSize, iterationCount);
    }

    public static Test createAddingToMiddleLinkedListTest(int listSize, int iterationCount){
        return new AddingToMiddleLinkedListTest(listSize, iterationCount);
    }

    public static Test createAddingToMiddleArrayList_10000_Test(int listSize){
        return new AddingToMiddleArrayListTest(listSize, 10_000);
    }

    public static Test createAddingToMiddleLinkedList_10000_Test(int listSize){
        return new AddingToMiddleLinkedListTest(listSize, 10_000);
    }

    public static Test createAddingToMiddleArrayList_20000_Test(int listSize){
        return new AddingToMiddleArrayListTest(listSize, 20_000);
    }

    public static Test createAddingToMiddleLinkedList_20000_Test(int listSize){
        return new AddingToMiddleLinkedListTest(listSize, 20_000);
    }

    private static abstract class AddingToMiddleListTest implements Test{

        protected final List list;
        protected final int listSize;
        protected final int iterationCount;

        private AddingToMiddleListTest(int listSize, int iterationCount, List list){
            this.list = list;
            this.listSize = listSize;
            this.iterationCount = iterationCount;
        }

        @Override
        public void executeOperation() {
            this.list.add(this.list.size() / 2, ELEMENT);
        }

        @Override
        public void doSetup() {
            System.out.println("Setup");
            System.out.println(String.format("Adding %s items to the list", this.listSize));
            this.list.addAll(Collections.nCopies(this.listSize,ELEMENT ));
            System.out.println("Setup complete");
        }

        @Override
        public void cleanup() {
            System.out.println("Clearing the list");
            this.list.clear();
        }

        @Override
        public int getIterationCount() {
            return iterationCount;
        }
    }

    private static class AddingToMiddleArrayListTest extends AddingToMiddleListTest {

        public AddingToMiddleArrayListTest(int lisSize, int iterationCount) {
            super(lisSize, iterationCount, new ArrayList());
        }

        @Override
        public String getName() {
            return "addElementToMiddle_ArrayList";
        }
    }

    private static class AddingToMiddleLinkedListTest extends AddingToMiddleListTest {

        public AddingToMiddleLinkedListTest(int listSize, int iterationCount) {
            super(listSize, iterationCount, new LinkedList());
        }

        @Override
        public String getName() {
            return "addElementToMiddle_LinkedList";
        }
    }
}
