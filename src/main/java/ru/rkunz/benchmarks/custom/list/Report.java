package ru.rkunz.benchmarks.custom.list;

/**
 * Created by rkunz on 03.04.2017.
 */
public class Report {

    private final String content;
    private final String name;

    public Report(String name, String content) {
        this.content = content;
        this.name = name;
    }

    public void print(){
        System.out.println(String.format("Result for test %s - %s",
                this.name, this.content));
    }
}
