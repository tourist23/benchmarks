package ru.rkunz.benchmarks.custom.list;

/**
 * Created by rkunz on 03.04.2017.
 */
public interface Test {

    String getName();

    void executeOperation();

    void doSetup();

    void cleanup();

    int getIterationCount();
}
