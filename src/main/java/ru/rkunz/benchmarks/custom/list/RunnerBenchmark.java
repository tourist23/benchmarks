package ru.rkunz.benchmarks.custom.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rkunz on 03.04.2017.
 */
public class RunnerBenchmark {

    private final static int LIST_SIZE = 1_000_000;

    private static void warm(Test test){
        System.out.println("****************************************************");
        System.out.println("Starting warm up");

        System.out.println(String.format("Test name: %s", test.getName()));
        System.out.println(String.format("Iteration count: %s", test.getIterationCount()));

        test.doSetup();

        for(int i = 0; i < test.getIterationCount(); i++){
            test.executeOperation();
        }

        test.cleanup();

        System.out.println("Warm up completion");
    }

    private static void measure(Test test){
        measureTotalTime(test);
        measureAvarageTime(test);
    }

    public static Report measureTotalTime(Test test){
        System.out.println("****************************************************");
        System.out.println("Starting measuring total time");
        System.out.println(String.format("Test name: %s", test.getName()));
        System.out.println(String.format("Iteration count: %s", test.getIterationCount()));

        test.doSetup();

        long startTime = System.nanoTime();
        for(int i = 0; i < test.getIterationCount(); i++){
            test.executeOperation();
        }
        long endTime = System.nanoTime();

        test.cleanup();
        System.out.println("Measuring completion");

        return new Report(
                test.getName(),
                String.format("Iteration: %s. Total time: %s ms", test.getIterationCount(), (endTime - startTime)/1000));
    }

    public static Report measureAvarageTime(Test test){
        System.out.println("****************************************************");
        System.out.println("Starting measuring average time");
        System.out.println(String.format("Test name: %s", test.getName()));
        System.out.println(String.format("Iteration count: %s", test.getIterationCount()));

        test.doSetup();

        long totalTime = 0;
        for(int i = 0; i < test.getIterationCount(); i++){
            long startTime = System.nanoTime();
            test.executeOperation();
            totalTime += System.nanoTime() - startTime;
        }

        test.cleanup();
        System.out.println("Measuring completion");

        return new Report(test.getName(),
                String.format("Iterations: %s. Average time: %s ms",
                        test.getIterationCount(), totalTime/test.getIterationCount()));
    }

    public static void printResults(List<Report> reports){
        System.out.println("******************************************************");
        for(Report report: reports){
            report.print();
        }
        System.out.println("******************************************************");
    }

    public static void main(String... args){
        List<Test> tests = Arrays.asList(
                AddingToListBenchmark.createAddingToMiddleArrayList_10000_Test(LIST_SIZE),
                AddingToListBenchmark.createAddingToMiddleLinkedList_10000_Test(LIST_SIZE),
                AddingToListBenchmark.createAddingToMiddleArrayList_20000_Test(LIST_SIZE),
                AddingToListBenchmark.createAddingToMiddleLinkedList_20000_Test(LIST_SIZE)
        );

        List<Report> reports = new ArrayList<>();

        for (Test test: tests){
            warm(test);
            reports.add(measureTotalTime(test));
            reports.add(measureAvarageTime(test));
        }

        printResults(reports);
    }
}
