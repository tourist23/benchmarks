package ru.rkunz.benchmarks.jmh.list;

import org.openjdk.jmh.annotations.*;

import java.util.List;

/**
 * Created by rkunz on 29.03.2017.
 */
@BenchmarkMode(Mode.SingleShotTime)
@Warmup(iterations = 5, batchSize = StateFixtures.BATCH_SIZE)
@Measurement(iterations = 5, batchSize = StateFixtures.BATCH_SIZE)
public class RemovingFromListBenchmark {

    @Benchmark
    public List removeElementFromBeginning_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.remove(0);
        return holder.list;
    }

    @Benchmark
    public List removeElementFromBeginning_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.remove(0);
        return holder.list;
    }

    @Benchmark
    public List removeElementFromMiddle_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.remove(holder.list.size() / 2);
        return holder.list;
    }

    @Benchmark
    public List removeElementFromMiddle_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.remove(holder.list.size() / 2);
        return holder.list;
    }

    @Benchmark
    public List removeElementFromMiddleThroughListIterator_LinkedList(StateFixtures.LinkedListHolder holder) {
        if(holder.listIteratorOnMiddlePosition.hasNext()){
            holder.listIteratorOnMiddlePosition.next();
            holder.listIteratorOnMiddlePosition.remove();
        }
        return holder.list;
    }

    @Benchmark
    public List removeElementFromEnd_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.remove(holder.list.size() - 1);
        return holder.list;
    }

    @Benchmark
    public List removeElementFromEnd_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.remove(holder.list.size() - 1);
        return holder.list;
    }
}
