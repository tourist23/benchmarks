package ru.rkunz.benchmarks.jmh.list;

import org.openjdk.jmh.annotations.*;

import java.util.List;

/**
 * Created by rkunz on 29.03.2017.
 */
@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 5)
@Measurement(iterations = 5)
public class GettingFromListBenchmark {

    @Benchmark
    public List getElementFromBeginning_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.get(0);
        return holder.list;
    }

    @Benchmark
    public List getElementFromBeginning_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.get(0);
        return holder.list;
    }

    @Benchmark
    public List getElementFromMiddle_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.get(holder.size / 2);
        return holder.list;
    }

    @Benchmark
    public List getElementFromMiddle_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.get(holder.size / 2);
        return holder.list;
    }

    @Benchmark
    public List getElementFromEnd_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.get(holder.size - 1 );
        return holder.list;
    }

    @Benchmark
    public List getElementFromEnd_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.get(holder.size - 1);
        return holder.list;
    }
}
