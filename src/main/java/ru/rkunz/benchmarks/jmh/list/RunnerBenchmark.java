package ru.rkunz.benchmarks.jmh.list;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/**
 * Created by rkunz on 29.03.2017.
 */
public class RunnerBenchmark {

    public static void main(String... args) throws RunnerException {
        Options opts = new OptionsBuilder()
                .include(".*ListBenchmark.*")
                .timeUnit(TimeUnit.MICROSECONDS)
                .forks(1)
                .build();
        new Runner(opts).run();
    }
}
