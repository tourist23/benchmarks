package ru.rkunz.benchmarks.jmh.list;

import org.openjdk.jmh.annotations.*;

import java.util.*;

/**
 * Created by rkunz on 29.03.2017.
 */
public class StateFixtures {

    public static final Object ELEMENT = new Object();
    public static final int BATCH_SIZE = 10_000;

    @State(Scope.Thread)
    public abstract static class Holder {

        @Param({"10000", "1000000"})
        int size;
        List list;

        @Setup(Level.Iteration)
        public abstract void doSetup();
    }

    public static class ArrayListHolder extends Holder {

        public void doSetup(){
            this.list = new ArrayList(Collections.nCopies(size,ELEMENT ));
        }
    }

    public static class ArrayListWithMoreCapacityHolder extends Holder {

        public void doSetup(){
            this.list = new ArrayList(size + BATCH_SIZE);
            this.list.addAll(Collections.nCopies(size,ELEMENT ));
        }
    }

    public static class LinkedListHolder extends Holder {

        ListIterator listIteratorOnMiddlePosition;

        public void doSetup(){
            this.list = new LinkedList(Collections.nCopies(size,ELEMENT ));
            this.listIteratorOnMiddlePosition = this.list.listIterator(size / 2);
        }
    }
}
