package ru.rkunz.benchmarks.jmh.list;

import org.openjdk.jmh.annotations.*;

import java.util.List;

/**
 * Created by rkunz on 29.03.2017.
 */
@BenchmarkMode(Mode.SingleShotTime)
@Warmup(iterations = 5, batchSize = StateFixtures.BATCH_SIZE)
@Measurement(iterations = 5, batchSize = StateFixtures.BATCH_SIZE)
public class AddingToListBenchmark {

    @Benchmark
    public List addElementToBeginning_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.add(0, StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToBeginning_ArrayListWithMoreCapacity(StateFixtures.ArrayListWithMoreCapacityHolder holder) {
        holder.list.add(0, StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToBeginning_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.add(0, StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToMiddle_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.add(holder.list.size() / 2, StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToMiddle_ArrayListWithMoreCapacity(StateFixtures.ArrayListWithMoreCapacityHolder holder) {
        holder.list.add(holder.list.size() / 2, StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToMiddle_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.add(holder.list.size() / 2, StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToMiddleThroughListIterator_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.listIteratorOnMiddlePosition.add(StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToEnd_ArrayList(StateFixtures.ArrayListHolder holder) {
        holder.list.add(StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToEnd_ArrayListWithMoreCapacity(StateFixtures.ArrayListWithMoreCapacityHolder holder) {
        holder.list.add(StateFixtures.ELEMENT);
        return holder.list;
    }

    @Benchmark
    public List addElementToEnd_LinkedList(StateFixtures.LinkedListHolder holder) {
        holder.list.add(StateFixtures.ELEMENT);
        return holder.list;
    }
}
